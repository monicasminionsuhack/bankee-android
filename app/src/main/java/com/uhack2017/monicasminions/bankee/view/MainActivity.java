package com.uhack2017.monicasminions.bankee.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.uhack2017.monicasminions.bankee.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
