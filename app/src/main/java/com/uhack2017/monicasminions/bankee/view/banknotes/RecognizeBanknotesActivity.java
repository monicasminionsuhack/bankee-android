package com.uhack2017.monicasminions.bankee.view.banknotes;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by monica on 12/2/17.
 */

public class RecognizeBanknotesActivity extends AppCompatActivity implements RecognizeBankNotesView {
    @Override
    public void onBankNoteRecognized(String message) {

    }

    @Override
    public void onBankNoteUnrecognized(String error) {

    }
}
