package com.uhack2017.monicasminions.bankee.view.banknotes;

/**
 * Created by monica on 12/2/17.
 */

public interface RecognizeBankNotesView {
    void onBankNoteRecognized(String message);
    void onBankNoteUnrecognized(String error);
}
