package com.uhack2017.monicasminions.bankee.presenter;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uhack2017.monicasminions.bankee.view.banknotes.RecognizeBankNotesView;

import java.util.Iterator;

/**
 * Created by monica on 12/2/17.
 */

public class RecognizeBankNotesPresenter {
    private RecognizeBankNotesView view;

    public void getSaints() {
        Log.d(RecognizeBankNotesPresenter.class.getSimpleName(), "getting from Firebase");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("sampledata/saints");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(RecognizeBankNotesPresenter.class.getSimpleName(), dataSnapshot.getValue().toString());
//                ArrayList<Saint> saints = new ArrayList<>();
                Iterator<DataSnapshot> response = dataSnapshot.getChildren().iterator();
                //TODO: Put this HashMap of data in arraylist of objects
                //https://stackoverflow.com/questions/44935343/how-to-iterate-over-a-datasnapshot
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(RecognizeBankNotesPresenter.class.getSimpleName(), databaseError.toString());
                databaseError.toException().printStackTrace();
                view.onBankNoteUnrecognized(databaseError.getMessage());
            }
        });

    }
}
